---
nome: História de usuário
sobre: Um breve histórico sobre a história de usuário

---

### Dados extras

Dados extra para a criação da história de usuário

### Critérios de Aceite

- [x] A história deve seguir um padrão
- [x] O *sobre* deve ser de fácil entendimento
