<?php

namespace Tests\Feature;

use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{

    public function testRequest()
    {
        $restClient = new \Finnet\RestClient("http://www.google.com.br/");

        $expected = "Google";
        $actual = $restClient->get();



        $this->assertEquals($expected, $actual['head']['title'], "Verificando o titulo do google");
    }


}