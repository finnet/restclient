<?php

/**
 * --------------------------------------------------------------------------
 *
 * --------------------------------------------------------------------------
 * PHP version 7.1
 *
 *
 * @category Libs
 * @package  RestClient
 * @author   Ercy Moreira Neto <ercy.neto@finnet.com.br>
 * @license  Property <http://www.finnet.com.br>
 * @link     http://www.finnet.com.br
 */
declare(strict_types=1);

namespace Finnet\Traits;

/**
 * Trait Factoriable
 */
trait Factoriable
{
    protected $binds = [];

    /**
     * @param $name
     * @param $args
     *
     * @return object
     * @throws \ReflectionException
     */
    protected function factory($name, $args)
    {
        $reflector = new \ReflectionClass($this->binds[$name]);
        return $reflector->newInstanceArgs($args);
    }
}