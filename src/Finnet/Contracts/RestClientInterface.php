<?php

/**
 * --------------------------------------------------------------------------
 *
 * --------------------------------------------------------------------------
 * PHP version 7.1
 *
 *
 * @category Libs
 * @package  RestClient
 * @author   Ercy Moreira Neto <ercy.neto@finnet.com.br>
 * @license  Property <http://www.finnet.com.br>
 * @link     http://www.finnet.com.br
 */
declare(strict_types=1);

namespace Finnet\Contracts;

/**
 * Interface RestClientInterface
 *
 * @package Finnet\Contracts
 */
interface RestClientInterface
{

    public function setHeader(array $param) : void;

    public function getUrl() : string;

    public function getBaseUrl() : string;

    public function setBaseUrl(string $url) : void;

    public function getErrors() : array;

    public function __call(string $name, array $arguments);
}