<?php
/**
 * --------------------------------------------------------------------------
 *
 * --------------------------------------------------------------------------
 * PHP version 7.1
 *
 *
 * @category Libs
 * @package  RestClient
 * @author   Ercy Moreira Neto <ercy.neto@finnet.com.br>
 * @license  Property <http://www.finnet.com.br>
 * @link     http://www.finnet.com.br
 */
declare(strict_types=1);

namespace Finnet\Contracts;

/**
 * Interface HttpRequest
 *
 * @package Finnet\Contracts
 */
interface HttpRequest
{
    public function setOption($name, $value) : void;
    public function execute();
    public function getInfo($name);
    public function getError();
    public function close() : void;
}