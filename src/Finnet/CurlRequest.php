<?php

/**
 * --------------------------------------------------------------------------
 *
 * --------------------------------------------------------------------------
 * PHP version 7.1
 *
 *
 * @category Libs
 * @package  RestClient
 * @author   Ercy Moreira Neto <ercy.neto@finnet.com.br>
 * @license  Property <http://www.finnet.com.br>
 * @link     http://www.finnet.com.br
 */
declare(strict_types=1);

namespace Finnet;

use Finnet\Contracts\HttpRequest;

/**
 * Class CurlRequest
 *
 * @package Finnet
 */
class CurlRequest implements HttpRequest
{
    private $handle = null;

    public function __construct($url)
    {
        $this->handle = curl_init($url);
    }

    public function setOption($name, $value) : void
    {
        curl_setopt($this->handle, $name, $value);
    }

    public function execute()
    {
        return curl_exec($this->handle);
    }

    public function getInfo($name)
    {
        return curl_getinfo($this->handle, $name);
    }

    public function getError()
    {
        return curl_error($this->handle);
    }

    public function close() : void
    {
        curl_close($this->handle);
    }

    function __destruct()
    {
        $this->close();
    }
}