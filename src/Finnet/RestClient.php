<?php

/**
 * --------------------------------------------------------------------------
 *
 * --------------------------------------------------------------------------
 * PHP version 7.1
 *
 *
 * @category Libs
 * @package  RestClient
 * @author   Ercy Moreira Neto <ercy.neto@finnet.com.br>
 * @license  Property <http://www.finnet.com.br>
 * @link     http://www.finnet.com.br
 */
declare(strict_types=1);

namespace Finnet;

use Finnet\Contracts\RestClientInterface;
use Finnet\Contracts\HttpRequest;
use Finnet\Traits\Factoriable;

/**
 * Classe client de consumo de APIs Restful
 * @example $attr = ['9eed65f52ca26de310cda76ce3b8862a290b8e7564417c863b0599d17ec1c54e'];
 * $body = '{"file" : "teste"}';
 * $teste = new RestClient('http://idp.finnet.local/ws/v1');
 * $teste->setHeader([]);
 * echo $teste->getToken([ 'body' => $body, 'attributes' => $attr]);
 */
class RestClient
    implements RestClientInterface
{
    use Factoriable;

	private $url = null;

	private $header = [
	    'Accept: application/json'
    ];
	
	private $method = null;

	
	private $attributes = [];
	
	private $body = '';

	private $error = [];
	
	/**
	 * Constructor Method
	 * @param string $url
	 * @return void
	 */
	public function __construct(string $url)
	{
		if(substr($url, -1) != '/') {
			$url .= '/';
		}
		
		$this->url = $url;

		$this->binds['CurlRequest'] = __NAMESPACE__ .  '\\CurlRequest';
	}

    /**
     * @param $url
     *
     * @return HttpRequest
     * @throws \ReflectionException
     */
	private function getCurlRequest($url) : HttpRequest
    {
        /**
         * @var HttpRequest $httpRequest
         */
        $httpRequest = $this->factory('CurlRequest', $url);
        return $httpRequest;
    }

	public function setHeader(array $param) : void
	{
		$this->header = $param;
	}


	private function setDataByName($param)
	{
		$pieces = preg_split('/(?=[A-Z])/', $param);
		
		if(!empty($pieces)) {
			$this->method = mb_strtoupper(array_shift($pieces));
			
			foreach($pieces as $item) {
				$this->attributes[] = mb_strtolower($item);
			}
		}
	}
	
	private function setDataByArguments($param)
	{
		$param = reset($param);
				
		if(isset($param['body'])) {

			$body = $param['body'];
			
			if($body != null) {
				if(is_object($body)) {
					$body = (array) $body;
				} elseif(is_string($body)) {
					$body = json_decode($body, true);
				}
				
				$this->body = json_encode($body);
			}
		}
		
		if(isset($param['attributes'])) {
			foreach($param['attributes'] as $item) {
				$this->attributes[] = mb_strtolower($item);
			}
		}
	}

	public function getBaseUrl(): string
    {
        return $this->url;
    }

    public function setBaseUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getUrl() : string
	{
		$url = $this->url;
	
		if(!empty($this->attributes)) {
			
			$url .= implode('/', $this->attributes);
		}
		
		return $url;
	}

	public function getErrors() : array
	{		
		return $this->error;
	}
	
	private function executeCurl()
	{
		$retorno = null;
		
		$url = $this->getUrl();


		$curlRequest = new CurlRequest($url);
		$curlRequest->setOption(CURLOPT_URL, $url);
		$curlRequest->setOption(CURLOPT_RETURNTRANSFER, true);

//		$ch = curl_init($url);
//
//		curl_setopt($ch, CURLOPT_URL, $url);
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		
		switch($this->method) {

			case 'POST':
				$merge = ['Content-Type: application/json','Content-Length: ' . strlen($this->body)];
				$this->header = array_merge($this->header, $merge);

                $curlRequest->setOption(CURLOPT_POST, $url);
                $curlRequest->setOption(CURLOPT_POSTFIELDS, $this->body);
				break;

			case 'PUT':
			case 'PATCH':
				$merge = ['Content-Type: application/json','Content-Length: ' . strlen($this->body)];
				$this->header = array_merge($this->header, $merge);
                $curlRequest->setOption(CURLOPT_POSTFIELDS, $this->body);
                $curlRequest->setOption(CURLOPT_CUSTOMREQUEST, $this->method);
				break;

			case 'DELETE':
                $curlRequest->setOption(CURLOPT_CUSTOMREQUEST, $this->method);
				break;
		}

		if(!empty($this->header)){
            $curlRequest->setOption(CURLOPT_HTTPHEADER, $this->header);
        }
		
		$retorno = $curlRequest->execute();

		
		$httpCode = $curlRequest->getInfo(CURLINFO_HTTP_CODE); //  curl_getinfo($ch, CURLINFO_HTTP_CODE);

		if( ($httpCode < 200 && $httpCode > 299) || (!empty($curlRequest->getError()) ) ) {

			$this->error = ['status_code' => $httpCode, 'error' => $curlRequest->getError() , 'response' => $retorno];

		} else {

			$retorno = $this->formatResponse($retorno);
		}
		
		return $retorno;
	}

    private function correct_encoding($text) {
        $current_encoding = mb_detect_encoding($text, 'auto');
        $text = iconv($current_encoding, 'UTF-8//IGNORE', $text);
        return $text;
    }

	private function formatResponse($response)
	{
		$retorno = [];

		if($response != '') {

			$retorno = json_decode($response, true);

			if($retorno == null) {
                $doc = new \DOMDocument();
                $doc->strictErrorChecking = FALSE;
                // set error level
                $internalErrors = libxml_use_internal_errors(true);
                $doc->loadHTML($this->correct_encoding($response));
                // Restore error level
                libxml_use_internal_errors($internalErrors);

                $xml = simplexml_import_dom($doc);
                $json = json_encode($xml);
                $retorno = json_decode($json,TRUE);
			}

			if($retorno == null) {
				$retorno['errors'][] = 'Não foi possível obter um retorno da requisição';
			}
		}

		return $retorno;
	}
	
	public function __call(string $name, array $arguments)
    {
        $this->setDataByName($name);
		$this->setDataByArguments($arguments);
		return $this->executeCurl();
    }
}
